import 'package:base_app/domain/user/user.dart';
import 'package:dartz/dartz.dart';
import 'package:base_app/domain/auth/auth_failure.dart';

abstract class IAuthFacade {
  Future<bool> isSignedIn();

  Future<Option<User>> getSignedInUser();

  Future<Either<AuthFailure, Unit>> loginWithGoogle();

  Future<void> signOut();
}
