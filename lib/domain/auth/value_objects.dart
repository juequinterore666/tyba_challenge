import 'package:dartz/dartz.dart';
import 'package:base_app/domain/core/failure.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/domain/core/value_validators.dart';

class EmailAddress extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory EmailAddress(String input) {
    assert(input != null);
    return EmailAddress._(validateEmailAddress(input));
  }

  EmailAddress._(this.value);
}
