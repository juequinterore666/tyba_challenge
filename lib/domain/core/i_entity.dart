import 'package:base_app/domain/core/value_object.dart';

abstract class IEntity {
  UniqueId get id;
}
