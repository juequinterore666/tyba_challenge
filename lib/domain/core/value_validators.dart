import 'package:dartz/dartz.dart';
import 'package:base_app/domain/core/failure.dart';
import 'package:string_validator/string_validator.dart';

Either<NumberNotInRange<T>, T> validateNumberInRange<T extends num>(
    T input, T minValue, T maxValue) {
  if (input >= minValue && input <= maxValue) {
    return right<NumberNotInRange<T>, T>(input);
  }
  return left<NumberNotInRange<T>, T>(ValueFailure.numberNotInRange(
      failedValue: input, min: minValue, max: maxValue));
}

Either<Multiline<String>, String> validateSingleLineString(String input) {
  if (input.contains('\n')) {
    return left(ValueFailure.multiline(failedValue: input));
  }
  return right(input);
}

Either<InvalidEmail<String>, String> validateEmailAddress(String input) {
  if (isEmail(input)) {
    return right(input);
  }

  return left(ValueFailure.invalidEmail(failedValue: input));
}
