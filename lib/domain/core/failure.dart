import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'failure.freezed.dart';

@freezed
abstract class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.empty({@required T failedValue}) = Empty<T>;

  const factory ValueFailure.numberNotInRange(
      {@required T failedValue,
      @required num min,
      @required num max}) = NumberNotInRange<T>;

  const factory ValueFailure.multiline({
    @required T failedValue,
  }) = Multiline<T>;

  const factory ValueFailure.invalidEmail({@required T failedValue}) =
      InvalidEmail<T>;
}
