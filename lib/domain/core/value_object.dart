import 'package:dartz/dartz.dart';
import 'package:base_app/domain/core/errors.dart';
import 'package:base_app/domain/core/failure.dart';
import 'package:base_app/domain/core/value_validators.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

@immutable
abstract class ValueObject<T> {
  const ValueObject();

  Either<ValueFailure<T>, T> get value;

  T getOrCrash() {
    // id = identity - same as writing (right) => right
    return value.fold((l) => throw UnexpectedValueError(l), id);
  }

  T getOrElse(T defaultValue) {
    return value.getOrElse(() => defaultValue);
  }

  Either<ValueFailure<dynamic>, Unit> get failureOrUnit =>
      value.fold((l) => left(l), (r) => right(unit));

  bool isValid() => value.isRight();

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is ValueObject<T> && other.value == value;

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'ValueObject($value)';
}

class UniqueId extends ValueObject<String> {
  static final UniqueId empty = UniqueId.fromUniqueString('');

  @override
  final Either<ValueFailure<String>, String> value;

  factory UniqueId() {
    return UniqueId._(right(Uuid().v4()));
  }

  factory UniqueId.fromUniqueString(String uniqueIdString) {
    assert(uniqueIdString != null);
    return UniqueId._(right(uniqueIdString));
  }

  const UniqueId._(this.value);
}

class SingleLineString extends ValueObject<String> {
  static final SingleLineString empty = SingleLineString('');

  @override
  final Either<ValueFailure<String>, String> value;

  factory SingleLineString(String input) {
    assert(input != null);
    return SingleLineString._(validateSingleLineString(input));
  }

  const SingleLineString._(this.value);
}
