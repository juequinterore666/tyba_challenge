import 'package:base_app/domain/user/user.dart';
import 'package:dartz/dartz.dart';

abstract class IUserRepository {
  Future<String> createUser(User user);

  Stream<Iterable<User>> getAllUsers();

  Stream<Option<User>> getUserById(String id);
}
