import 'package:flutter/foundation.dart';
import 'package:base_app/domain/core/i_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/domain/auth/value_objects.dart';

part 'user.freezed.dart';

@freezed
abstract class User with _$User implements IEntity {
  static final empty = User(
      id: UniqueId.empty,
      email: EmailAddress('none@none.com'),
      name: SingleLineString.empty);

  const factory User(
      {@required UniqueId id,
      @required EmailAddress email,
      SingleLineString name}) = _User;
}
