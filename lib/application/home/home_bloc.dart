import 'package:base_app/domain/home/home_failure.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_app/domain/auth/i_auth_facade.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_bloc_state.dart';

part 'home_bloc_event.dart';

part 'home_bloc.freezed.dart';

@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final IAuthFacade _authFacade;

  HomeBloc(this._authFacade) : super(HomeState.initial());

  @override
  Stream<HomeState> mapEventToState(event) async* {
    yield* event.map(initialize: (_Initialize value) async* {
      final isLogged = await _authFacade.isSignedIn();
      yield state.copyWith(isLogged: isLogged, failureOrSuccessOption: none());
    });
  }
}
