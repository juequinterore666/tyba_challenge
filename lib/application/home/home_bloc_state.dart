part of 'home_bloc.dart';

@freezed
abstract class HomeState with _$HomeState {
  const factory HomeState(
          {@required bool isLoading,
          @required bool isLogged,
          @required Option<Either<HomeFailure, Unit>> failureOrSuccessOption}) =
      _HomeState;

  factory HomeState.initial() => HomeState(
      isLoading: true, isLogged: true, failureOrSuccessOption: none());
}
