import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_app/domain/auth/auth_failure.dart';
import 'package:base_app/domain/auth/i_auth_facade.dart';

part 'login_bloc_state.dart';

part 'login_bloc_event.dart';

part 'login_bloc.freezed.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final IAuthFacade _authFacade;

  LoginBloc(this._authFacade) : super(LoginState.initial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    yield* event.map(login: (_Login loginEvent) async* {
      yield state.copyWith(isLoading: true);
      final failureOrSuccess = await _authFacade.loginWithGoogle();
      var isLogged = false;
      if (failureOrSuccess.isRight()) {
        isLogged = true;
      }
      yield state.copyWith(
          isLoading: false,
          isLogged: isLogged,
          failureOrSuccessOption: some(failureOrSuccess));
    });
  }
}
