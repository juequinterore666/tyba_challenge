part of 'login_bloc.dart';

@freezed
abstract class LoginState with _$LoginState {
  const factory LoginState(
          {@required bool isLoading,
          @required bool isLogged,
          @required Option<Either<AuthFailure, Unit>> failureOrSuccessOption}) =
      _LoginState;

  factory LoginState.initial() => LoginState(
      isLoading: false, isLogged: false, failureOrSuccessOption: none());
}

@module
abstract class LoginBlocStateInjectableModule {
  @lazySingleton
  LoginState get initial => LoginState.initial();
}
