// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'infrastructure/auth/firebase_auth_facade.dart';
import 'infrastructure/core/firebase_injectable_module.dart';
import 'infrastructure/core/data_source/db/firestore_database.dart';
import 'application/home/home_bloc.dart';
import 'domain/auth/i_auth_facade.dart';
import 'infrastructure/core/data_source/db/i_firestore_crud.dart';
import 'application/login/login_bloc.dart';
import 'infrastructure/user/user_firestore_data_source.dart';
import 'infrastructure/user/user_repository.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final firebaseInjectableModule = _$FirebaseInjectableModule();
  final loginBlocStateInjectableModule = _$LoginBlocStateInjectableModule();
  gh.lazySingleton<FirebaseAuth>(() => firebaseInjectableModule.firebaseAuth);
  gh.lazySingleton<Firestore>(() => firebaseInjectableModule.firestore);
  gh.lazySingleton<GoogleSignIn>(() => firebaseInjectableModule.googleSignIn);
  gh.lazySingleton<IFirestoreCrud>(() => FirestoreCrud(get<Firestore>()));
  gh.lazySingleton<LoginState>(() => loginBlocStateInjectableModule.initial);
  gh.lazySingleton<FirestoreDatabase>(
      () => FirestoreDatabase(get<IFirestoreCrud>()));
  gh.factory<UserFirestoreDataSource>(
      () => UserFirestoreDataSource(get<FirestoreDatabase>()));
  gh.factory<UserRepository>(
      () => UserRepository(get<UserFirestoreDataSource>()));
  gh.lazySingleton<IAuthFacade>(() => FirebaseAuthFacade(
        get<FirebaseAuth>(),
        get<GoogleSignIn>(),
        get<UserRepository>(),
      ));
  gh.factory<LoginBloc>(() => LoginBloc(get<IAuthFacade>()));
  gh.factory<HomeBloc>(() => HomeBloc(get<IAuthFacade>()));
  return get;
}

class _$FirebaseInjectableModule extends FirebaseInjectableModule {}

class _$LoginBlocStateInjectableModule extends LoginBlocStateInjectableModule {}
