import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:base_app/infrastructure/core/data_source/db/firestore_database.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_database_facade.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_model.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_model_data_source.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';
import 'package:meta/meta.dart';
import 'package:base_app/infrastructure/core/data_source/db/utils.dart';

abstract class IFirestoreModelDataSource<T extends IModel>
    extends IModelDataSource<T> {
  final FirestoreDatabase _firestoreDatabase;
  final Map<String, Option<Object>> _nextSkips = {};

  IFirestoreModelDataSource(
      {@required String table, @required FirestoreDatabase firestoreDatabase})
      : _firestoreDatabase = firestoreDatabase,
        super(table);

  @override
  Future<String> create(T model) {
    final modelJson = model.toJson();
    modelJson['creationDate'] = FieldValue.serverTimestamp();
    modelJson['isDeleted'] = false;

    return _firestoreDatabase.create(table: table, model: modelJson);
  }

  @override
  Stream<Option<T>> model(String id) {
    return _firestoreDatabase.model(table: table, id: id).map((optionalJSON) =>
        optionalJSON.fold(() => none(), (json) => some(fromJson(json))));
  }

  @override
  Stream<Iterable<T>> modelsWhere(Iterable<IQueryClause> queryClauses,
      {bool ignoreDeleted = true}) {
    final newQueryClauses = List.of(queryClauses);

    if (ignoreDeleted) {
      newQueryClauses.add(const IQueryClause.where(
          comparator: QueryClauseComparator.equals,
          value: false,
          field: 'isDeleted'));
    }
    return _firestoreDatabase
        .modelsWhere(table: table, queryClauses: newQueryClauses)
        .map((paginatedModels) =>
            _storeNextSkipAndReturnModels(newQueryClauses, paginatedModels))
        .map((event) => event.map((e) => fromJson(e)));
  }

  @override
  Stream<Iterable<T>> next(Iterable<IQueryClause> queryClauses) {
    final queryId = buildQueryId(table, queryClauses);
    final nextSkip = _nextSkips[queryId];
    final allQueryClauses = List.of(queryClauses);

    allQueryClauses.add(IQueryClause.skip(exclusivePlaceToStart: nextSkip));

    return modelsWhere(allQueryClauses);
  }

  @override
  Future<void> update({String id, Map<String, dynamic> dataToUpdate}) {
    dataToUpdate['lastUpdatedDate'] = FieldValue.serverTimestamp();
    return _firestoreDatabase.update(table: table, id: id, model: dataToUpdate);
  }

  @override
  Future<void> delete(String id) {
    return update(id: id, dataToUpdate: {
      'isDeleted': true,
      'deletedDate': FieldValue.serverTimestamp()
    });
  }

  void _storeNextSkip(
      Iterable<IQueryClause> queryClauses, PaginatedModels paginatedModels) {
    final queryId = buildQueryId(table, queryClauses);
    final nextSkip = paginatedModels.nextSkip;
    _nextSkips[queryId] = nextSkip;
  }

  Iterable<Map<String, dynamic>> _storeNextSkipAndReturnModels(
      Iterable<IQueryClause> queryClauses, PaginatedModels paginatedModels) {
    _storeNextSkip(queryClauses, paginatedModels);
    return paginatedModels.models;
  }
}
