import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

String buildQueryId(String table, Iterable<IQueryClause> queryClauses) {
  final StringBuffer idBuffer = queryClauses.fold<StringBuffer>(
      StringBuffer(table), (StringBuffer previousValue, IQueryClause element) {
    previousValue.write('_$element');
    return previousValue;
  });

  return idBuffer.toString();
}
