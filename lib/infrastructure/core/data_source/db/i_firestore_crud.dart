import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

abstract class IFirestoreCrud {
  CollectionReference getCollection({@required String path});

  String generateId(String path);

  Future<void> createWithCustomId(
      {@required String path,
      @required String id,
      @required Map<String, dynamic> model});

  Future<void> upsert(
      {@required String path,
      @required String id,
      @required Map<String, dynamic> model});

  Future<String> create(
      {@required String path, @required Map<String, dynamic> model});

  Stream<DocumentSnapshot> document(
      {@required String path, @required String id});

  Stream<QuerySnapshot> documentsWhere(
      {@required String path, @required Iterable<IQueryClause> queryClauses});

  Future<void> update(
      {@required String path,
      @required String id,
      @required Map<String, dynamic> model});

  Future<void> delete({@required String path, @required String id});

  Future<void> deleteWhere(
      {@required String path, @required Iterable<IQueryClause> queryClauses});
}

@LazySingleton(as: IFirestoreCrud)
class FirestoreCrud implements IFirestoreCrud {
  final Firestore _firestore;

  FirestoreCrud(this._firestore);

  @override
  String generateId(String path) =>
      getCollection(path: path).document().documentID;

  @override
  CollectionReference getCollection({String path}) {
    return _firestore.collection(path);
  }

  @override
  Future<void> upsert(
      {String path, String id, Map<String, dynamic> model}) async {
    await _documentReference(path: path, id: id).setData(model, merge: true);
  }

  @override
  Future<void> createWithCustomId(
      {@required String path,
      @required String id,
      @required Map<String, dynamic> model}) async {
    await _documentReference(path: path, id: id).setData(model);
  }

  @override
  Future<String> create(
      {@required String path, @required Map<String, dynamic> model}) async {
    final docRef = await getCollection(path: path).add(model);
    return docRef.documentID;
  }

  @override
  Stream<DocumentSnapshot> document(
      {@required String path, @required String id}) {
    return _documentReference(path: path, id: id).snapshots();
  }

  @override
  Stream<QuerySnapshot> documentsWhere(
      {@required String path, @required Iterable<IQueryClause> queryClauses}) {
    final collectionReference = getCollection(path: path);

    return _buildQuery(collectionReference, queryClauses).snapshots();
  }

  @override
  Future<void> update(
      {@required String path,
      @required String id,
      @required Map<String, dynamic> model}) async {
    await _documentReference(path: path, id: id).updateData(model);
  }

  @override
  Future<void> delete({@required String path, @required String id}) async {
    _documentReference(path: path, id: id).delete();
  }

  @override
  Future<void> deleteWhere(
      {String path, Iterable<IQueryClause> queryClauses}) async {
    final querySnapshot =
        await documentsWhere(path: path, queryClauses: queryClauses).first;
    await _firestore.runTransaction(
      (transaction) async {
        final documents =
            querySnapshot.documents.where((element) => element.exists);
        for (final document in documents) {
          transaction.delete(document.reference);
        }
      },
    );
  }

  DocumentReference _documentReference(
      {@required String path, @required String id}) {
    return getCollection(path: path).document(id);
  }

  Query _buildQuery(Query initialQuery, Iterable<IQueryClause> queryClauses) {
    final finalQuery = queryClauses.fold(initialQuery,
        (Query previousValue, IQueryClause element) {
      return element.map(
          limit: (limit) => previousValue.limit(limit.amount),
          skip: (skip) => previousValue.startAfterDocument(
              skip.exclusivePlaceToStart as DocumentSnapshot),
          sort: (sort) => previousValue.orderBy(sort.field,
              descending: sort.order == SortQueryClauseOrder.desc),
          where: (where) => _buildWhereQuery(previousValue, where));
    });

    return finalQuery;
  }

  Query _buildWhereQuery(Query initialQuery, Where whereQueryClause) {
    switch (whereQueryClause.comparator) {
      case QueryClauseComparator.equals:
        return initialQuery.where(whereQueryClause.field,
            isEqualTo: whereQueryClause.value);
      case QueryClauseComparator.greaterThan:
        return initialQuery.where(whereQueryClause.field,
            isGreaterThan: whereQueryClause.value);
      case QueryClauseComparator.greaterThanOrEqual:
        return initialQuery.where(whereQueryClause.field,
            isGreaterThanOrEqualTo: whereQueryClause.value);
      case QueryClauseComparator.lessThan:
        return initialQuery.where(whereQueryClause.field,
            isLessThan: whereQueryClause.value);
      case QueryClauseComparator.lesThanOrEqual:
        return initialQuery.where(whereQueryClause.field,
            isLessThanOrEqualTo: whereQueryClause.value);
      default:
        throw UnsupportedError('Firestore does not support notEquals query');
    }
  }
}
