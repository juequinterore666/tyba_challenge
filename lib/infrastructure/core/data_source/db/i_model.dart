import 'package:cloud_firestore/cloud_firestore.dart';

abstract class IModel<D> {
  final String id;
  final bool isDeleted;
  final D creationDate;
  final D deletionDate;
  final D lastUpdatedDate;

  IModel({this.id, this.isDeleted = false, this.creationDate, this.deletionDate, this.lastUpdatedDate});

  Map<String, dynamic> toJson();
}

abstract class IFirestoreModel extends IModel<Timestamp> {
  static Timestamp jsonToTimestamp(dynamic timestamp) => timestamp as Timestamp;

  static DateTime timestampToJson(Timestamp timestamp) => timestamp?.toDate();
}
