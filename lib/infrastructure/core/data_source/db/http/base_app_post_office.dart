import 'package:dartz/dartz.dart';
import 'package:post_office/post_office.dart';

class BaseAppPostOffice
    extends PostOffice<Either<DeliveryError, DeliveryResponse>> {
  BaseAppPostOffice()
      : super(
            cityAddress: "https://cat-fact.herokuapp.com",
            defaultHeaders: {"content-type": "application/json"},
            deliveryManFactory: <T>(
                    {Package<T> package, PostOfficeConfig config}) =>
                HttpDeliveryMan<
                        T,
                        Either<DeliveryError,
                            DeliveryResponse<Map<String, dynamic>>>>(
                    config: config,
                    package: package,
                    responseParser: EitherHttpDeliveryManJsonResponseParser()));

  static test() async {
    final postOffice = BaseAppPostOffice();

    final package = Package.get("/facts/58e009550aac31001185ed12");

    final response = await postOffice.deliver(package);
    response.fold((l) => null, (r) => print("Respuesta: ${r.body["text"]}"));
  }
}
