import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';
import 'package:meta/meta.dart';

class PaginatedModels {
  final Iterable<Map<String, dynamic>> _models;
  final Option<Object> _nextSkip;

  Iterable<Map<String, dynamic>> get models => List.unmodifiable(_models);

  Option<Object> get nextSkip => _nextSkip;

  PaginatedModels({Iterable<Map<String, dynamic>> models, Object nextSkip})
      : _models = models,
        _nextSkip = nextSkip == null ? none() : some(nextSkip);
}

abstract class IDatabaseFacade {
  static final Map<String, dynamic> databaseNullModel = Map.unmodifiable({});

  Stream<Option<Map<String, dynamic>>> model(
      {@required String table, @required String id});

  Stream<PaginatedModels> modelsWhere(
      {@required String table, Iterable<IQueryClause> queryClauses = const {}});

  Future<String> create(
      {@required String table, @required Map<String, dynamic> model});

  Future<void> update(
      {@required String table,
      @required String id,
      @required Map<String, dynamic> model});

  Future<void> delete({@required String table, @required String id});
}
