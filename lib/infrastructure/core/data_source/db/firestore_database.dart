import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_database_facade.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_firestore_crud.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';
import 'package:base_app/infrastructure/core/data_source/db/utils.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';

@lazySingleton
class FirestoreDatabase extends IDatabaseFacade {
  final IFirestoreCrud _firestoreCrud;
  final Map<String, BehaviorSubject<PaginatedModels>> _listBehaviorSubjects =
      {};
  final Map<String, BehaviorSubject<Option<Map<String, dynamic>>>>
      _singleBehaviorSubjects = {};

  FirestoreDatabase(IFirestoreCrud firebaseCrud)
      : _firestoreCrud = firebaseCrud;

  @override
  Future<String> create(
      {@required String table, @required Map<String, dynamic> model}) async {
    final filteredModel = Map.of(model);
    filteredModel.removeWhere((key, value) => value == null);

    final existingAndNotEmptyId =
        model.containsKey('id') && model['id'].toString().isNotEmpty;

    final documentId = existingAndNotEmptyId
        ? model['id'] as String
        : _firestoreCrud.generateId(table);

    filteredModel['id'] = documentId;

    await _firestoreCrud.createWithCustomId(
        path: table, id: documentId, model: filteredModel);

    return documentId;
  }

  @override
  Stream<PaginatedModels> modelsWhere(
      {@required String table,
      Iterable<IQueryClause> queryClauses = const {}}) {
    final behaviorSubjectId = buildQueryId(table, queryClauses);

    if (_listBehaviorSubjects.containsKey(behaviorSubjectId)) {
      return _listBehaviorSubjects[behaviorSubjectId];
    }

    final behaviorSubject = BehaviorSubject<PaginatedModels>();
    _firestoreCrud
        .documentsWhere(path: table, queryClauses: queryClauses)
        .listen((event) {
      final lastDocumentSnapshot =
          event.documents.isNotEmpty ? event.documents.last : null;
      final allDocumentsData = event.documents.map((e) => e.data);
      behaviorSubject.add(PaginatedModels(
          models: allDocumentsData, nextSkip: lastDocumentSnapshot));
    });

    _listBehaviorSubjects[behaviorSubjectId] = behaviorSubject;

    return behaviorSubject;
  }

  @override
  Stream<Option<Map<String, dynamic>>> model(
      {@required String table, @required String id}) {
    final queryClause = IQueryClause.where(
        comparator: QueryClauseComparator.equals, field: 'id', value: id);

    final behaviorSubjectId = buildQueryId(table, [queryClause]);

    if (_singleBehaviorSubjects.containsKey(behaviorSubjectId)) {
      return _singleBehaviorSubjects[behaviorSubjectId];
    }

    final behaviorSubject = BehaviorSubject<Option<Map<String, dynamic>>>();
    modelsWhere(table: table, queryClauses: [queryClause])
        .listen((paginatedModel) {
      final models = paginatedModel.models;
      final Option<Map<String, dynamic>> model =
          models.isNotEmpty ? some(models.first) : none();

      behaviorSubject.add(model);
    });

    _singleBehaviorSubjects[behaviorSubjectId] = behaviorSubject;

    return behaviorSubject;
  }

  @override
  Future<void> update(
      {@required String table,
      @required String id,
      @required Map<String, dynamic> model}) {
    return _firestoreCrud.update(path: table, id: id, model: model);
  }

  @override
  Future<void> delete({@required String table, @required String id}) async {
    return _firestoreCrud.delete(path: table, id: id);
  }
}
