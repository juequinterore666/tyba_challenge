import 'package:freezed_annotation/freezed_annotation.dart';

part 'i_query_clause.freezed.dart';

enum QueryClauseComparator {
  equals,
  greaterThan,
  greaterThanOrEqual,
  lessThan,
  lesThanOrEqual,
  notEquals,
}

enum QueryClauseKind {
  limit,
  skip,
  sort,
  where,
}

enum SortQueryClauseOrder { asc, desc }

@freezed
abstract class IQueryClause with _$IQueryClause {
  const factory IQueryClause.limit({int amount}) = _Limit;

  const factory IQueryClause.skip({dynamic exclusivePlaceToStart}) = _Skip;

  const factory IQueryClause.sort({SortQueryClauseOrder order, String field}) =
      _Sort;

  const factory IQueryClause.where(
      {QueryClauseComparator comparator, String field, dynamic value}) = Where;
}
