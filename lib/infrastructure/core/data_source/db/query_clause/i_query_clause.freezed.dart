// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'i_query_clause.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$IQueryClauseTearOff {
  const _$IQueryClauseTearOff();

// ignore: unused_element
  _Limit limit({int amount}) {
    return _Limit(
      amount: amount,
    );
  }

// ignore: unused_element
  _Skip skip({dynamic exclusivePlaceToStart}) {
    return _Skip(
      exclusivePlaceToStart: exclusivePlaceToStart,
    );
  }

// ignore: unused_element
  _Sort sort({SortQueryClauseOrder order, String field}) {
    return _Sort(
      order: order,
      field: field,
    );
  }

// ignore: unused_element
  Where where({QueryClauseComparator comparator, String field, dynamic value}) {
    return Where(
      comparator: comparator,
      field: field,
      value: value,
    );
  }
}

// ignore: unused_element
const $IQueryClause = _$IQueryClauseTearOff();

mixin _$IQueryClause {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result limit(int amount),
    @required Result skip(dynamic exclusivePlaceToStart),
    @required Result sort(SortQueryClauseOrder order, String field),
    @required
        Result where(
            QueryClauseComparator comparator, String field, dynamic value),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result limit(int amount),
    Result skip(dynamic exclusivePlaceToStart),
    Result sort(SortQueryClauseOrder order, String field),
    Result where(QueryClauseComparator comparator, String field, dynamic value),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result limit(_Limit value),
    @required Result skip(_Skip value),
    @required Result sort(_Sort value),
    @required Result where(Where value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result limit(_Limit value),
    Result skip(_Skip value),
    Result sort(_Sort value),
    Result where(Where value),
    @required Result orElse(),
  });
}

abstract class $IQueryClauseCopyWith<$Res> {
  factory $IQueryClauseCopyWith(
          IQueryClause value, $Res Function(IQueryClause) then) =
      _$IQueryClauseCopyWithImpl<$Res>;
}

class _$IQueryClauseCopyWithImpl<$Res> implements $IQueryClauseCopyWith<$Res> {
  _$IQueryClauseCopyWithImpl(this._value, this._then);

  final IQueryClause _value;
  // ignore: unused_field
  final $Res Function(IQueryClause) _then;
}

abstract class _$LimitCopyWith<$Res> {
  factory _$LimitCopyWith(_Limit value, $Res Function(_Limit) then) =
      __$LimitCopyWithImpl<$Res>;
  $Res call({int amount});
}

class __$LimitCopyWithImpl<$Res> extends _$IQueryClauseCopyWithImpl<$Res>
    implements _$LimitCopyWith<$Res> {
  __$LimitCopyWithImpl(_Limit _value, $Res Function(_Limit) _then)
      : super(_value, (v) => _then(v as _Limit));

  @override
  _Limit get _value => super._value as _Limit;

  @override
  $Res call({
    Object amount = freezed,
  }) {
    return _then(_Limit(
      amount: amount == freezed ? _value.amount : amount as int,
    ));
  }
}

class _$_Limit implements _Limit {
  const _$_Limit({this.amount});

  @override
  final int amount;

  @override
  String toString() {
    return 'IQueryClause.limit(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Limit &&
            (identical(other.amount, amount) ||
                const DeepCollectionEquality().equals(other.amount, amount)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(amount);

  @override
  _$LimitCopyWith<_Limit> get copyWith =>
      __$LimitCopyWithImpl<_Limit>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result limit(int amount),
    @required Result skip(dynamic exclusivePlaceToStart),
    @required Result sort(SortQueryClauseOrder order, String field),
    @required
        Result where(
            QueryClauseComparator comparator, String field, dynamic value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return limit(amount);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result limit(int amount),
    Result skip(dynamic exclusivePlaceToStart),
    Result sort(SortQueryClauseOrder order, String field),
    Result where(QueryClauseComparator comparator, String field, dynamic value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (limit != null) {
      return limit(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result limit(_Limit value),
    @required Result skip(_Skip value),
    @required Result sort(_Sort value),
    @required Result where(Where value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return limit(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result limit(_Limit value),
    Result skip(_Skip value),
    Result sort(_Sort value),
    Result where(Where value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (limit != null) {
      return limit(this);
    }
    return orElse();
  }
}

abstract class _Limit implements IQueryClause {
  const factory _Limit({int amount}) = _$_Limit;

  int get amount;
  _$LimitCopyWith<_Limit> get copyWith;
}

abstract class _$SkipCopyWith<$Res> {
  factory _$SkipCopyWith(_Skip value, $Res Function(_Skip) then) =
      __$SkipCopyWithImpl<$Res>;
  $Res call({dynamic exclusivePlaceToStart});
}

class __$SkipCopyWithImpl<$Res> extends _$IQueryClauseCopyWithImpl<$Res>
    implements _$SkipCopyWith<$Res> {
  __$SkipCopyWithImpl(_Skip _value, $Res Function(_Skip) _then)
      : super(_value, (v) => _then(v as _Skip));

  @override
  _Skip get _value => super._value as _Skip;

  @override
  $Res call({
    Object exclusivePlaceToStart = freezed,
  }) {
    return _then(_Skip(
      exclusivePlaceToStart: exclusivePlaceToStart == freezed
          ? _value.exclusivePlaceToStart
          : exclusivePlaceToStart as dynamic,
    ));
  }
}

class _$_Skip implements _Skip {
  const _$_Skip({this.exclusivePlaceToStart});

  @override
  final dynamic exclusivePlaceToStart;

  @override
  String toString() {
    return 'IQueryClause.skip(exclusivePlaceToStart: $exclusivePlaceToStart)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Skip &&
            (identical(other.exclusivePlaceToStart, exclusivePlaceToStart) ||
                const DeepCollectionEquality().equals(
                    other.exclusivePlaceToStart, exclusivePlaceToStart)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(exclusivePlaceToStart);

  @override
  _$SkipCopyWith<_Skip> get copyWith =>
      __$SkipCopyWithImpl<_Skip>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result limit(int amount),
    @required Result skip(dynamic exclusivePlaceToStart),
    @required Result sort(SortQueryClauseOrder order, String field),
    @required
        Result where(
            QueryClauseComparator comparator, String field, dynamic value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return skip(exclusivePlaceToStart);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result limit(int amount),
    Result skip(dynamic exclusivePlaceToStart),
    Result sort(SortQueryClauseOrder order, String field),
    Result where(QueryClauseComparator comparator, String field, dynamic value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (skip != null) {
      return skip(exclusivePlaceToStart);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result limit(_Limit value),
    @required Result skip(_Skip value),
    @required Result sort(_Sort value),
    @required Result where(Where value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return skip(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result limit(_Limit value),
    Result skip(_Skip value),
    Result sort(_Sort value),
    Result where(Where value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (skip != null) {
      return skip(this);
    }
    return orElse();
  }
}

abstract class _Skip implements IQueryClause {
  const factory _Skip({dynamic exclusivePlaceToStart}) = _$_Skip;

  dynamic get exclusivePlaceToStart;
  _$SkipCopyWith<_Skip> get copyWith;
}

abstract class _$SortCopyWith<$Res> {
  factory _$SortCopyWith(_Sort value, $Res Function(_Sort) then) =
      __$SortCopyWithImpl<$Res>;
  $Res call({SortQueryClauseOrder order, String field});
}

class __$SortCopyWithImpl<$Res> extends _$IQueryClauseCopyWithImpl<$Res>
    implements _$SortCopyWith<$Res> {
  __$SortCopyWithImpl(_Sort _value, $Res Function(_Sort) _then)
      : super(_value, (v) => _then(v as _Sort));

  @override
  _Sort get _value => super._value as _Sort;

  @override
  $Res call({
    Object order = freezed,
    Object field = freezed,
  }) {
    return _then(_Sort(
      order: order == freezed ? _value.order : order as SortQueryClauseOrder,
      field: field == freezed ? _value.field : field as String,
    ));
  }
}

class _$_Sort implements _Sort {
  const _$_Sort({this.order, this.field});

  @override
  final SortQueryClauseOrder order;
  @override
  final String field;

  @override
  String toString() {
    return 'IQueryClause.sort(order: $order, field: $field)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Sort &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.field, field) ||
                const DeepCollectionEquality().equals(other.field, field)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(field);

  @override
  _$SortCopyWith<_Sort> get copyWith =>
      __$SortCopyWithImpl<_Sort>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result limit(int amount),
    @required Result skip(dynamic exclusivePlaceToStart),
    @required Result sort(SortQueryClauseOrder order, String field),
    @required
        Result where(
            QueryClauseComparator comparator, String field, dynamic value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return sort(order, field);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result limit(int amount),
    Result skip(dynamic exclusivePlaceToStart),
    Result sort(SortQueryClauseOrder order, String field),
    Result where(QueryClauseComparator comparator, String field, dynamic value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (sort != null) {
      return sort(order, field);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result limit(_Limit value),
    @required Result skip(_Skip value),
    @required Result sort(_Sort value),
    @required Result where(Where value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return sort(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result limit(_Limit value),
    Result skip(_Skip value),
    Result sort(_Sort value),
    Result where(Where value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (sort != null) {
      return sort(this);
    }
    return orElse();
  }
}

abstract class _Sort implements IQueryClause {
  const factory _Sort({SortQueryClauseOrder order, String field}) = _$_Sort;

  SortQueryClauseOrder get order;
  String get field;
  _$SortCopyWith<_Sort> get copyWith;
}

abstract class $WhereCopyWith<$Res> {
  factory $WhereCopyWith(Where value, $Res Function(Where) then) =
      _$WhereCopyWithImpl<$Res>;
  $Res call({QueryClauseComparator comparator, String field, dynamic value});
}

class _$WhereCopyWithImpl<$Res> extends _$IQueryClauseCopyWithImpl<$Res>
    implements $WhereCopyWith<$Res> {
  _$WhereCopyWithImpl(Where _value, $Res Function(Where) _then)
      : super(_value, (v) => _then(v as Where));

  @override
  Where get _value => super._value as Where;

  @override
  $Res call({
    Object comparator = freezed,
    Object field = freezed,
    Object value = freezed,
  }) {
    return _then(Where(
      comparator: comparator == freezed
          ? _value.comparator
          : comparator as QueryClauseComparator,
      field: field == freezed ? _value.field : field as String,
      value: value == freezed ? _value.value : value as dynamic,
    ));
  }
}

class _$Where implements Where {
  const _$Where({this.comparator, this.field, this.value});

  @override
  final QueryClauseComparator comparator;
  @override
  final String field;
  @override
  final dynamic value;

  @override
  String toString() {
    return 'IQueryClause.where(comparator: $comparator, field: $field, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Where &&
            (identical(other.comparator, comparator) ||
                const DeepCollectionEquality()
                    .equals(other.comparator, comparator)) &&
            (identical(other.field, field) ||
                const DeepCollectionEquality().equals(other.field, field)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(comparator) ^
      const DeepCollectionEquality().hash(field) ^
      const DeepCollectionEquality().hash(value);

  @override
  $WhereCopyWith<Where> get copyWith =>
      _$WhereCopyWithImpl<Where>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result limit(int amount),
    @required Result skip(dynamic exclusivePlaceToStart),
    @required Result sort(SortQueryClauseOrder order, String field),
    @required
        Result where(
            QueryClauseComparator comparator, String field, dynamic value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return where(comparator, field, value);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result limit(int amount),
    Result skip(dynamic exclusivePlaceToStart),
    Result sort(SortQueryClauseOrder order, String field),
    Result where(QueryClauseComparator comparator, String field, dynamic value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (where != null) {
      return where(comparator, field, value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result limit(_Limit value),
    @required Result skip(_Skip value),
    @required Result sort(_Sort value),
    @required Result where(Where value),
  }) {
    assert(limit != null);
    assert(skip != null);
    assert(sort != null);
    assert(where != null);
    return where(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result limit(_Limit value),
    Result skip(_Skip value),
    Result sort(_Sort value),
    Result where(Where value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (where != null) {
      return where(this);
    }
    return orElse();
  }
}

abstract class Where implements IQueryClause {
  const factory Where(
      {QueryClauseComparator comparator,
      String field,
      dynamic value}) = _$Where;

  QueryClauseComparator get comparator;
  String get field;
  dynamic get value;
  $WhereCopyWith<Where> get copyWith;
}
