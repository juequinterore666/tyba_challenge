import 'package:dartz/dartz.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_model.dart';
import 'package:meta/meta.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

abstract class IModelDataSource<T extends IModel> {
  final String table;

  IModelDataSource(this.table);

  Future<String> create(T model);

  Stream<Option<T>> model(String id);

  Stream<Iterable<T>> modelsWhere(Iterable<IQueryClause> iQueryClauses);

  Stream<Iterable<T>> next(Iterable<IQueryClause> iQueryClauses);

  Future<void> update(
      {@required String id, @required Map<String, dynamic> dataToUpdate});

  Future<void> delete(String id);

  T fromJson(Map<String, dynamic> json);
}
