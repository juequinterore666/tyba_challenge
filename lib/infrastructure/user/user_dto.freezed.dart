// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
UserDto _$UserDtoFromJson(Map<String, dynamic> json) {
  return _UserDto.fromJson(json);
}

class _$UserDtoTearOff {
  const _$UserDtoTearOff();

// ignore: unused_element
  _UserDto call(
      {@required
          String id,
      @required
          String email,
      String name,
      bool isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp lastUpdatedDate}) {
    return _UserDto(
      id: id,
      email: email,
      name: name,
      isDeleted: isDeleted,
      creationDate: creationDate,
      deletionDate: deletionDate,
      lastUpdatedDate: lastUpdatedDate,
    );
  }
}

// ignore: unused_element
const $UserDto = _$UserDtoTearOff();

mixin _$UserDto {
  String get id;
  String get email;
  String get name;
  bool get isDeleted;
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get creationDate;
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get deletionDate;
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get lastUpdatedDate;

  Map<String, dynamic> toJson();
  $UserDtoCopyWith<UserDto> get copyWith;
}

abstract class $UserDtoCopyWith<$Res> {
  factory $UserDtoCopyWith(UserDto value, $Res Function(UserDto) then) =
      _$UserDtoCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String email,
      String name,
      bool isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp lastUpdatedDate});
}

class _$UserDtoCopyWithImpl<$Res> implements $UserDtoCopyWith<$Res> {
  _$UserDtoCopyWithImpl(this._value, this._then);

  final UserDto _value;
  // ignore: unused_field
  final $Res Function(UserDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object email = freezed,
    Object name = freezed,
    Object isDeleted = freezed,
    Object creationDate = freezed,
    Object deletionDate = freezed,
    Object lastUpdatedDate = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      email: email == freezed ? _value.email : email as String,
      name: name == freezed ? _value.name : name as String,
      isDeleted: isDeleted == freezed ? _value.isDeleted : isDeleted as bool,
      creationDate: creationDate == freezed
          ? _value.creationDate
          : creationDate as Timestamp,
      deletionDate: deletionDate == freezed
          ? _value.deletionDate
          : deletionDate as Timestamp,
      lastUpdatedDate: lastUpdatedDate == freezed
          ? _value.lastUpdatedDate
          : lastUpdatedDate as Timestamp,
    ));
  }
}

abstract class _$UserDtoCopyWith<$Res> implements $UserDtoCopyWith<$Res> {
  factory _$UserDtoCopyWith(_UserDto value, $Res Function(_UserDto) then) =
      __$UserDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String email,
      String name,
      bool isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp lastUpdatedDate});
}

class __$UserDtoCopyWithImpl<$Res> extends _$UserDtoCopyWithImpl<$Res>
    implements _$UserDtoCopyWith<$Res> {
  __$UserDtoCopyWithImpl(_UserDto _value, $Res Function(_UserDto) _then)
      : super(_value, (v) => _then(v as _UserDto));

  @override
  _UserDto get _value => super._value as _UserDto;

  @override
  $Res call({
    Object id = freezed,
    Object email = freezed,
    Object name = freezed,
    Object isDeleted = freezed,
    Object creationDate = freezed,
    Object deletionDate = freezed,
    Object lastUpdatedDate = freezed,
  }) {
    return _then(_UserDto(
      id: id == freezed ? _value.id : id as String,
      email: email == freezed ? _value.email : email as String,
      name: name == freezed ? _value.name : name as String,
      isDeleted: isDeleted == freezed ? _value.isDeleted : isDeleted as bool,
      creationDate: creationDate == freezed
          ? _value.creationDate
          : creationDate as Timestamp,
      deletionDate: deletionDate == freezed
          ? _value.deletionDate
          : deletionDate as Timestamp,
      lastUpdatedDate: lastUpdatedDate == freezed
          ? _value.lastUpdatedDate
          : lastUpdatedDate as Timestamp,
    ));
  }
}

@JsonSerializable()
class _$_UserDto extends _UserDto {
  const _$_UserDto(
      {@required
          this.id,
      @required
          this.email,
      this.name,
      this.isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          this.creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          this.deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          this.lastUpdatedDate})
      : assert(id != null),
        assert(email != null),
        super._();

  factory _$_UserDto.fromJson(Map<String, dynamic> json) =>
      _$_$_UserDtoFromJson(json);

  @override
  final String id;
  @override
  final String email;
  @override
  final String name;
  @override
  final bool isDeleted;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  final Timestamp creationDate;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  final Timestamp deletionDate;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  final Timestamp lastUpdatedDate;

  @override
  String toString() {
    return 'UserDto(id: $id, email: $email, name: $name, isDeleted: $isDeleted, creationDate: $creationDate, deletionDate: $deletionDate, lastUpdatedDate: $lastUpdatedDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _UserDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.isDeleted, isDeleted) ||
                const DeepCollectionEquality()
                    .equals(other.isDeleted, isDeleted)) &&
            (identical(other.creationDate, creationDate) ||
                const DeepCollectionEquality()
                    .equals(other.creationDate, creationDate)) &&
            (identical(other.deletionDate, deletionDate) ||
                const DeepCollectionEquality()
                    .equals(other.deletionDate, deletionDate)) &&
            (identical(other.lastUpdatedDate, lastUpdatedDate) ||
                const DeepCollectionEquality()
                    .equals(other.lastUpdatedDate, lastUpdatedDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(isDeleted) ^
      const DeepCollectionEquality().hash(creationDate) ^
      const DeepCollectionEquality().hash(deletionDate) ^
      const DeepCollectionEquality().hash(lastUpdatedDate);

  @override
  _$UserDtoCopyWith<_UserDto> get copyWith =>
      __$UserDtoCopyWithImpl<_UserDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserDtoToJson(this);
  }
}

abstract class _UserDto extends UserDto {
  const _UserDto._() : super._();
  const factory _UserDto(
      {@required
          String id,
      @required
          String email,
      String name,
      bool isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp lastUpdatedDate}) = _$_UserDto;

  factory _UserDto.fromJson(Map<String, dynamic> json) = _$_UserDto.fromJson;

  @override
  String get id;
  @override
  String get email;
  @override
  String get name;
  @override
  bool get isDeleted;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get creationDate;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get deletionDate;
  @override
  @JsonKey(
      fromJson: IFirestoreModel.jsonToTimestamp,
      toJson: IFirestoreModel.timestampToJson)
  Timestamp get lastUpdatedDate;
  @override
  _$UserDtoCopyWith<_UserDto> get copyWith;
}
