import 'package:injectable/injectable.dart';
import 'package:base_app/infrastructure/core/data_source/db/firestore_database.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_firestore_model_data_source.dart';
import 'package:base_app/infrastructure/user/user_dto.dart';

@injectable
class UserFirestoreDataSource extends IFirestoreModelDataSource<UserDto> {
  UserFirestoreDataSource(FirestoreDatabase firestoreDatabase)
      : super(table: 'users', firestoreDatabase: firestoreDatabase);

  @override
  UserDto fromJson(Map<String, dynamic> json) => UserDto.fromJson(json);
}
