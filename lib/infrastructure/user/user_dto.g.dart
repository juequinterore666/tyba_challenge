// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserDto _$_$_UserDtoFromJson(Map<String, dynamic> json) {
  return _$_UserDto(
    id: json['id'] as String,
    email: json['email'] as String,
    name: json['name'] as String,
    isDeleted: json['isDeleted'] as bool,
    creationDate: IFirestoreModel.jsonToTimestamp(json['creationDate']),
    deletionDate: IFirestoreModel.jsonToTimestamp(json['deletionDate']),
    lastUpdatedDate: IFirestoreModel.jsonToTimestamp(json['lastUpdatedDate']),
  );
}

Map<String, dynamic> _$_$_UserDtoToJson(_$_UserDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'name': instance.name,
      'isDeleted': instance.isDeleted,
      'creationDate': IFirestoreModel.timestampToJson(instance.creationDate),
      'deletionDate': IFirestoreModel.timestampToJson(instance.deletionDate),
      'lastUpdatedDate':
          IFirestoreModel.timestampToJson(instance.lastUpdatedDate),
    };
