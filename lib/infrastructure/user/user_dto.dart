import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:base_app/domain/user/user.dart';
import 'package:base_app/domain/auth/value_objects.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_model.dart';

part 'user_dto.freezed.dart';

part 'user_dto.g.dart';

@freezed
abstract class UserDto implements _$UserDto, IFirestoreModel {
  const UserDto._();

  const factory UserDto(
      {@required
          String id,
      @required
          String email,
      String name,
      bool isDeleted,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp creationDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp deletionDate,
      @JsonKey(fromJson: IFirestoreModel.jsonToTimestamp, toJson: IFirestoreModel.timestampToJson)
          Timestamp lastUpdatedDate}) = _UserDto;

  factory UserDto.fromDomain(User user) => UserDto(
      id: user.id.getOrCrash(),
      email: user.email.getOrCrash(),
      name: user.name.getOrCrash());

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);
}

extension UserDtoX on UserDto {
  User toDomain() => User(
      id: UniqueId.fromUniqueString(id),
      name: SingleLineString(name),
      email: EmailAddress(email));
}
