import 'package:base_app/domain/user/user.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:base_app/domain/user/i_user_repository.dart';
import 'package:base_app/infrastructure/user/user_dto.dart';
import 'package:base_app/infrastructure/user/user_firestore_data_source.dart';

@injectable
class UserRepository extends IUserRepository {
  final UserFirestoreDataSource _userFirestoreDataSource;

  UserRepository(UserFirestoreDataSource userFirestoreDataSource)
      : _userFirestoreDataSource = userFirestoreDataSource;

  @override
  Future<String> createUser(User user) =>
      _userFirestoreDataSource.create(UserDto.fromDomain(user));

  @override
  Stream<Iterable<User>> getAllUsers() => _userFirestoreDataSource.modelsWhere(
      []).map((userDtos) => userDtos.map((userDto) => userDto.toDomain()));

  @override
  Stream<Option<User>> getUserById(String id) =>
      _userFirestoreDataSource.model(id).map((optionalUserDto) =>
          optionalUserDto.map((userDto) => userDto.toDomain()));
}
