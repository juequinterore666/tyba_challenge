import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:base_app/domain/auth/auth_failure.dart';
import 'package:base_app/domain/auth/i_auth_facade.dart';
import 'package:base_app/domain/user/user.dart';
import 'package:base_app/domain/auth/value_objects.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/infrastructure/user/user_repository.dart';

@LazySingleton(as: IAuthFacade)
class FirebaseAuthFacade implements IAuthFacade {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;
  final UserRepository _userRepository;

  FirebaseAuthFacade(
      this._firebaseAuth, this._googleSignIn, this._userRepository) {
    this._firebaseAuth.onAuthStateChanged.distinct().listen((user) {
      userAuthStateChanged(user);
    });
  }

  @override
  Future<bool> isSignedIn() async {
    final authUser = await _firebaseAuth.currentUser();
    return authUser != null;
  }

  @override
  Future<Option<User>> getSignedInUser() async {
    final authUser = await _firebaseAuth.currentUser();
    if (authUser != null) {
      await _userRepository.getUserById(authUser.uid).first;
    }
    return authUser == null
        ? none()
        : await _userRepository.getUserById(authUser.uid).first;
  }

  @override
  Future<Either<AuthFailure, Unit>> loginWithGoogle() async {
    try {
      final user = await _googleSignIn.signIn();
      if (user == null) {
        return left(AuthFailure.cancelledByUser());
      }

      final googleAuthentication = await user.authentication;
      final authCredential = GoogleAuthProvider.getCredential(
          idToken: googleAuthentication.idToken,
          accessToken: googleAuthentication.accessToken);

      await _firebaseAuth.signInWithCredential(authCredential);
      return right(unit);
    } on PlatformException {
      return left(AuthFailure.serverError());
    }
  }

  @override
  Future<void> signOut() async {
    Future.wait([_googleSignIn.signOut(), _firebaseAuth.signOut()]);
  }

  userAuthStateChanged(FirebaseUser user) async {
    if (user == null) {
      signOut();
      return;
    }

    final loggedInUser = await _userRepository.getUserById(user.uid).first;
    loggedInUser.fold(() => createRegisteredUser(user), (_) => null);
  }

  createRegisteredUser(FirebaseUser firebaseUser) async {
    final uid = UniqueId.fromUniqueString(firebaseUser.uid);
    final displayName = SingleLineString(firebaseUser.displayName);
    final email = EmailAddress(firebaseUser.email);

    final user = User(id: uid, email: email, name: displayName);

    await _userRepository.createUser(user);
  }
}
