import 'package:auto_route/auto_route_annotations.dart';
import 'package:base_app/presentation/home/home_page.dart';
import 'package:base_app/presentation/login/login_page.dart';

@MaterialAutoRouter(
    generateNavigationHelperExtension: true,
    routes: <AutoRoute>[
      MaterialRoute(page: LoginPage),
      MaterialRoute(page: HomePage, initial: true),
    ])
class $Router {}
