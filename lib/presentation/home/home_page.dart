import 'package:auto_route/auto_route.dart';
import 'package:base_app/application/home/home_bloc.dart';
import 'package:base_app/injection.dart';
import 'package:base_app/presentation/core/widgets/points_loader.dart';
import 'package:base_app/routes/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Base App"),
      ),
      body: BlocProvider(
        create: (context) => getIt<HomeBloc>()..add(HomeEvent.initialize()),
        child: HomeContent(),
      ),
    );
  }
}

class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
        builder: (context, state) => Container(),
        listener: (BuildContext context, HomeState state) {
          if (state.isLoading) {
            showDialog(
                context: context,
                builder: (_) => PointsLoader.random(),
                barrierDismissible: false);
          }
          if (!state.isLogged) {
            ExtendedNavigator.of(context).popAndPush(Routes.loginPage);
          }
        });
  }
}
