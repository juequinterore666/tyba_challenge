import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:base_app/routes/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_app/application/login/login_bloc.dart';
import 'package:base_app/domain/auth/auth_failure.dart';
import 'package:base_app/injection.dart';
import 'package:base_app/presentation/core/widgets/points_loader.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
          create: (context) => getIt<LoginBloc>(), child: LoginForm()),
    );
  }
}

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (BuildContext context, LoginState state) {
        if (state.isLoading) {
          showDialog(
              context: context,
              builder: (_) => PointsLoader.random(),
              barrierDismissible: false);
        }
        if (state.isLogged) {
          ExtendedNavigator.of(context).pop();
        }

        state.failureOrSuccessOption.fold(
            () {},
            (either) => either.fold((authFailure) {
                  final SnackBar snackBar = authFailureToSnackBar(authFailure);
                  Navigator.of(context, rootNavigator: true).pop();
                  Scaffold.of(context).showSnackBar(snackBar);
                }, (_) => Navigator.of(context, rootNavigator: true).pop()));
      },
      builder: (context, state) => Container(
        color: Color.fromARGB(255, 51, 52, 53),
        child: Center(
          child: OutlineButton(
            onPressed: () {
              BlocProvider.of<LoginBloc>(context).add(LoginEvent.login());
            },
            borderSide: BorderSide(color: Colors.white, width: 2),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 45),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            color: Colors.redAccent,
            child: Text(
              'Login with Google',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ),
      ),
    );
  }

  SnackBar authFailureToSnackBar(AuthFailure authFailure) {
    return authFailure.map(
      cancelledByUser: (_) =>
          const SnackBar(content: Text('Interaction cancelled by user')),
      serverError: (_) => const SnackBar(content: Text('Server error')),
      emailAlreadyInUse: (_) =>
          const SnackBar(content: Text('Email already in use')),
    );
  }
}
