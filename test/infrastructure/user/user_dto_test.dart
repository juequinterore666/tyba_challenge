import 'package:base_app/domain/auth/value_objects.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/domain/user/user.dart';
import 'package:base_app/infrastructure/user/user_dto.dart';

void main() {
  test('should create UserDto from User in fromDomain', () {
    final user = User(
        id: UniqueId.fromUniqueString('anUniqueId'),
        name: SingleLineString('aName'),
        email: EmailAddress('juequinterore666@gmail.com'));

    const expectedUserDto = UserDto(
        id: 'anUniqueId', email: 'juequinterore666@gmail.com', name: 'aName');
    expect(expectedUserDto, equals(UserDto.fromDomain(user)));
  });

  test('should create User from UserDto in toDomain', () {
    final expectedUser = User(
        id: UniqueId.fromUniqueString('anUniqueId'),
        name: SingleLineString('aName'),
        email: EmailAddress('juequinterore666@gmail.com'));

    const userDto = UserDto(
        id: 'anUniqueId', email: 'juequinterore666@gmail.com', name: 'aName');

    expect(expectedUser, equals(userDto.toDomain()));
  });
}
