import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:base_app/domain/auth/auth_failure.dart';
import 'package:base_app/domain/user/user.dart';
import 'package:base_app/domain/auth/value_objects.dart';
import 'package:base_app/domain/core/value_object.dart';
import 'package:base_app/infrastructure/auth/firebase_auth_facade.dart';
import 'package:base_app/infrastructure/user/user_repository.dart';
import 'package:mockito/mockito.dart';

class MockFirebaseAuth extends Mock implements FirebaseAuth {
  bool _loggedIn;

  MockFirebaseAuth([this._loggedIn = true]);

  @override
  Stream<FirebaseUser> get onAuthStateChanged async* {
    yield _loggedIn ? MockFirebaseUser() : null;
  }
}

class MockFirebaseUser extends Mock implements FirebaseUser {
  @override
  String get uid => 'anUid';

  @override
  String get displayName => 'Simelemón Tolomeo';

  @override
  String get email => 'anEmail@anEmail.com';
}

class MockGoogleSignIn extends Mock implements GoogleSignIn {}

class MockGoogleSignInAccount extends Mock implements GoogleSignInAccount {}

class MockUserRepository extends Mock implements UserRepository {}

class MockGoogleSignInAuthentication extends Fake
    implements GoogleSignInAuthentication {
  @override
  String get idToken => "anIdToken";

  @override
  String get accessToken => "anAccessToken";

  @override
  String get serverAuthCode => "aServerAuthCode";
}

void main() {
  FirebaseAuthFacade authFacade;
  MockFirebaseAuth _mockFirebaseAuth;
  MockGoogleSignIn _mockGoogleSignIn;
  MockUserRepository _mockUserRepository;

  final User _user = User(
      id: UniqueId.fromUniqueString('anUid'),
      name: SingleLineString('Simelemón Tolomeo'),
      email: EmailAddress('anEmail@anEmail.com'));

  setUpAll(() {
    _mockGoogleSignIn = MockGoogleSignIn();
    _mockFirebaseAuth = MockFirebaseAuth();
    _mockUserRepository = MockUserRepository();

    when(_mockUserRepository.getUserById('anUid')).thenAnswer((realInvocation) {
      return Stream.value(some(_user));
    });

    authFacade = FirebaseAuthFacade(
        _mockFirebaseAuth, _mockGoogleSignIn, _mockUserRepository);
  });

  group("GoogleSignIn", () {
    test('should call GoogleSignIn.signIn when logging with Google', () {
      authFacade.loginWithGoogle();

      verify(_mockGoogleSignIn.signIn());
    });

    test('should return AuthFailure.cancelledByUser if user is null', () async {
      when(_mockGoogleSignIn.signIn()).thenAnswer((realInvocation) => null);

      final loginResult = await authFacade.loginWithGoogle();

      expect(loginResult, left(AuthFailure.cancelledByUser()));
    });

    test(
        'should return AuthFailure.serverError if there was a PlatformException',
        () async {
      when(_mockGoogleSignIn.signIn())
          .thenThrow(PlatformException(code: 'aCode'));

      final loginResult = await authFacade.loginWithGoogle();

      expect(loginResult, left(AuthFailure.serverError()));
    });

    test(
        'should call FirebaseAuth signInWithCredentials if google sign in was successful',
        () async {
      final googleSignInAuthentication = MockGoogleSignInAuthentication();
      final googleSignInAccount = MockGoogleSignInAccount();
      when(googleSignInAccount.authentication)
          .thenAnswer((_) => Future.value(googleSignInAuthentication));
      when(_mockGoogleSignIn.signIn())
          .thenAnswer((_) => Future.value(googleSignInAccount));

      final loginResult = await authFacade.loginWithGoogle();

      verify(_mockFirebaseAuth.signInWithCredential(captureThat(predicate((a) =>
          a.idToken == 'anIdToken' && a.accessToken == 'anAccessToken'))));
      expect(loginResult, right(unit));
    });
  });

  group("SignedInUser", () {
    test('should call firebaseAuth currentUser', () {
      authFacade.getSignedInUser();

      verify(_mockFirebaseAuth.currentUser()).called(1);
    });

    test('should return none() if user is not logged in', () async {
      when(_mockFirebaseAuth.currentUser())
          .thenAnswer((realInvocation) => null);

      final signedInUser = await authFacade.getSignedInUser();

      verify(_mockFirebaseAuth.currentUser()).called(1);
      expect(signedInUser, equals(none()));
    });

    test('should call UserRepository getId if user is logged in', () async {
      final MockFirebaseUser firebaseUser = MockFirebaseUser();
      final user = User(
          id: UniqueId.fromUniqueString('anUid'),
          name: SingleLineString('aName'),
          email: EmailAddress('anEmail'));
      when(_mockFirebaseAuth.currentUser())
          .thenAnswer((realInvocation) => Future.value(firebaseUser));
      when(_mockUserRepository.getUserById('anUid')).thenAnswer(
          (realInvocation) => Stream.fromFuture(Future.value(some(user))));

      await authFacade.getSignedInUser();

      verify(_mockUserRepository.getUserById('anUid'));
    });
  });

  group('userAuthStateChanged', () {
    test('should call signOut when userAuthStateChanged is called with null',
        () async {
      _mockGoogleSignIn = MockGoogleSignIn();
      _mockFirebaseAuth = MockFirebaseAuth(false);
      _mockUserRepository = MockUserRepository();

      when(_mockGoogleSignIn.signOut()).thenAnswer(
          (realInvocation) => Future.value(MockGoogleSignInAccount()));

      when(_mockFirebaseAuth.signOut()).thenAnswer((_) async {});

      authFacade = FirebaseAuthFacade(
          _mockFirebaseAuth, _mockGoogleSignIn, _mockUserRepository);

      await Future.delayed(Duration(seconds: 0));

      verify(_mockGoogleSignIn.signOut()).called(1);
      verify(_mockFirebaseAuth.signOut()).called(1);
    });

    test('should call signOut when userAuthStateChanged is called with null',
        () async {
      _mockGoogleSignIn = MockGoogleSignIn();
      _mockFirebaseAuth = MockFirebaseAuth(false);
      _mockUserRepository = MockUserRepository();

      when(_mockGoogleSignIn.signOut()).thenAnswer(
          (realInvocation) => Future.value(MockGoogleSignInAccount()));

      when(_mockFirebaseAuth.signOut()).thenAnswer((_) async {});

      authFacade = FirebaseAuthFacade(
          _mockFirebaseAuth, _mockGoogleSignIn, _mockUserRepository);

      await Future.delayed(Duration(seconds: 0));

      verify(_mockGoogleSignIn.signOut()).called(1);
      verify(_mockFirebaseAuth.signOut()).called(1);
    });

    test(
        'should call createUser with created user if does not exists in Firestore',
        () async {
      _mockGoogleSignIn = MockGoogleSignIn();
      _mockFirebaseAuth = MockFirebaseAuth();
      _mockUserRepository = MockUserRepository();

      when(_mockUserRepository.getUserById('anUid'))
          .thenAnswer((realInvocation) {
        return Stream.value(none());
      });

      authFacade = FirebaseAuthFacade(
          _mockFirebaseAuth, _mockGoogleSignIn, _mockUserRepository);

      await Future.delayed(Duration(seconds: 0));

      verify(_mockUserRepository.createUser(_user)).called(1);
    });
  });
}
