import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:base_app/infrastructure/core/data_source/db/firestore_database.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_firestore_crud.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

class MockQuerySnapshotStream extends Mock implements Stream<QuerySnapshot> {}

class MockFirestoreCrud extends Mock implements IFirestoreCrud {}

void main() {
  group('Verify stream memoization', () {
    MockFirestoreCrud firestoreCrud;
    FirestoreDatabase firestoreDatabase;

    setUp(() {
      firestoreCrud = MockFirestoreCrud();
      firestoreDatabase = FirestoreDatabase(firestoreCrud);
    });

    test(
        'should return same instance if requesting model with same id and same table',
        () {
      when(firestoreCrud.documentsWhere(
              path: 'users', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((_) => MockQuerySnapshotStream());
      final subject1 = firestoreDatabase.model(table: 'users', id: 'anId');
      final subject2 = firestoreDatabase.model(table: 'users', id: 'anId');

      expect(subject1, equals(subject2));
    });

    test(
        'should return same instance if requesting same IQueryClauses and table',
        () {
      when(firestoreCrud.documentsWhere(
              path: 'users', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((_) => MockQuerySnapshotStream());
      const iQueryClauses = [
        IQueryClause.where(
            value: 25,
            comparator: QueryClauseComparator.greaterThan,
            field: 'age'),
        IQueryClause.limit(amount: 255),
        IQueryClause.sort(field: 'age', order: SortQueryClauseOrder.desc),
      ];
      final subject1 = firestoreDatabase.modelsWhere(
          table: 'users', queryClauses: iQueryClauses);
      final subject2 = firestoreDatabase.modelsWhere(
          table: 'users', queryClauses: iQueryClauses);

      expect(subject1, equals(subject2));
    });
  });

  group('Verify calls to firestoreCrud', () {
    MockFirestoreCrud firestoreCrud;
    FirestoreDatabase firestoreDatabase;

    setUp(() {
      firestoreCrud = MockFirestoreCrud();
      firestoreDatabase = FirestoreDatabase(firestoreCrud);
    });

    test(
        'should call firestoreCrud with generatedId and model with the generatedId inside when creating a new model without custom id',
        () {
      const user = {
        'email': 'juequinterore666@gmail.com',
        'age': 27,
      };
      when(firestoreCrud.generateId('users')).thenReturn('aGeneratedId');
      firestoreDatabase.create(table: 'users', model: user);
      final expectedUser = Map.of(user);
      expectedUser['id'] = 'aGeneratedId';

      verify(firestoreCrud.createWithCustomId(
          path: 'users',
          id: argThat(equals('aGeneratedId'), named: 'id'),
          model: expectedUser));
    });

    test(
        'should call firestoreCrud with generatedId when creating a new model with empty id',
        () {
      when(firestoreCrud.generateId('users')).thenReturn('aGeneratedId');
      const user = {'email': 'juequinterore666@gmail.com', 'age': 27, 'id': ''};
      firestoreDatabase.create(table: 'users', model: user);
      const expectedUser = {
        'email': 'juequinterore666@gmail.com',
        'age': 27,
        'id': 'aGeneratedId'
      };
      verify(firestoreCrud.createWithCustomId(
          path: 'users',
          id: argThat(equals('aGeneratedId'), named: 'id'),
          model: expectedUser));
    });

    test('should call firestoreCrud when creating a new model with custom id',
        () {
      const user = {
        'id': '1',
        'email': 'juequinterore666@gmail.com',
        'age': 27
      };
      firestoreDatabase.create(table: 'users', model: user);
      verify(firestoreCrud.createWithCustomId(
          path: 'users', id: '1', model: user));
    });

    test('should call firestoreCrud with null fields filtered with custom id',
        () {
      const user = {
        'email': 'juequinterore666@gmail.com',
        'age': null,
        'id': 'aCustomId'
      };
      const expectedUser = {
        'email': 'juequinterore666@gmail.com',
        'id': 'aCustomId'
      };

      firestoreDatabase.create(table: 'users', model: user);
      verify(firestoreCrud.createWithCustomId(
          path: 'users', id: 'aCustomId', model: expectedUser));
    });

    test(
        'should call firestoreCrud with null fields filtered without custom id',
        () {
      const user = {
        'email': 'juequinterore666@gmail.com',
        'age': null,
      };
      const expectedUser = {
        'email': 'juequinterore666@gmail.com',
        'id': 'aGeneratedId'
      };

      when(firestoreCrud.generateId('users')).thenReturn('aGeneratedId');
      firestoreDatabase.create(table: 'users', model: user);
      verify(firestoreCrud.createWithCustomId(
          path: 'users',
          id: argThat(equals('aGeneratedId'), named: 'id'),
          model: expectedUser));
    });

    test('should call firestoreCrud when requesting one model', () {
      when(firestoreCrud.documentsWhere(
              path: 'users', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((_) => MockQuerySnapshotStream());
      firestoreDatabase.model(table: 'users', id: 'anId');
      const queryClause = IQueryClause.where(
          value: 'anId', comparator: QueryClauseComparator.equals, field: 'id');
      verify(firestoreCrud
          .documentsWhere(path: 'users', queryClauses: [queryClause]));
    });

    test('should call firestoreCrud when deleting model', () {
      firestoreDatabase.delete(table: 'users', id: '1');

      verify(firestoreCrud.delete(path: 'users', id: '1'));
    });

    test('should call firestoreCrud when requesting multiple models', () {
      when(firestoreCrud.documentsWhere(
              path: 'users', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((_) => MockQuerySnapshotStream());

      const queryClause = IQueryClause.where(
          value: 34,
          comparator: QueryClauseComparator.greaterThan,
          field: 'age');

      firestoreDatabase
          .modelsWhere(table: 'users', queryClauses: [queryClause]);

      verify(firestoreCrud
          .documentsWhere(path: 'users', queryClauses: [queryClause]));
    });

    test('should call firestoreCrud when updating model', () {
      const model = {'age': 100};
      firestoreDatabase.update(table: 'users', id: '1', model: model);

      verify(firestoreCrud.update(path: 'users', id: '1', model: model));
    });
  });

  group('Verify model result', () {
    FirestoreDatabase firestoreDatabase;

    const users = [
      {'id': '1', 'email': 'juequinterore666@gmail.com', 'age': 35},
      {'id': '2', 'email': 'juequinterore6662@gmail.com', 'age': 27},
      {'id': '3', 'email': 'juequinterore6663@gmail.com', 'age': 36},
      {'id': '4', 'email': 'juequinterore6664@gmail.com', 'age': 37},
    ];

    setUp(() {
      final mockFirestore = MockFirestoreInstance();
      final firestoreCrud = FirestoreCrud(mockFirestore);
      firestoreDatabase = FirestoreDatabase(firestoreCrud);

      for (final user in users) {
        mockFirestore
            .collection('users')
            .document(user['id'] as String)
            .setData(user);
      }
    });

    test('should return generated id if creating model without custom id',
        () async {
      const user = {'email': 'juequinterore666100@gmail.com', 'age': 27};
      final generatedId =
          await firestoreDatabase.create(table: 'users', model: user);

      expect(generatedId, isNotNull);
    });

    test('should return original id when creating a new model with custom id',
        () async {
      const user = {
        'id': '100',
        'email': 'juequinterore666100@gmail.com',
        'age': 27
      };
      final id = await firestoreDatabase.create(table: 'users', model: user);

      expect(id, '100');
    });

    test('should return document map when requesting model', () async {
      firestoreDatabase.model(table: 'users', id: '2').listen((event) {
        expect(event.getOrElse(() => null),
            equals(users.where((element) => element['id'] == '2').first));
      });
    });

    test(
        'should return document maps and nextSkip with some when requesting multiple models and the query returned values',
        () async {
      const queryClauses = [
        IQueryClause.where(
            value: 34,
            comparator: QueryClauseComparator.greaterThan,
            field: 'age'),
        IQueryClause.limit(amount: 2)
      ];

      firestoreDatabase
          .modelsWhere(table: 'users', queryClauses: queryClauses)
          .listen((event) {
        expect(event.nextSkip, isA<Some>());
        expect((event.nextSkip.getOrElse(() => null) as DocumentSnapshot).data,
            equals(users.firstWhere((element) => element['id'] == '3')));
        expect(
            event.models.toList(),
            equals(
                users.where((element) => element['age'] as int > 34).take(2)));
      });
    });

    test(
        "should return document maps and nextSkip with none when requesting multiple models and the query didn't return values",
        () async {
      const queryClauses = [
        IQueryClause.where(
            value: 100,
            comparator: QueryClauseComparator.greaterThan,
            field: 'age'),
        IQueryClause.limit(amount: 5)
      ];

      firestoreDatabase
          .modelsWhere(table: 'users', queryClauses: queryClauses)
          .listen((event) {
        expect(event.nextSkip, isA<None>());
      });
    });
  });
}
