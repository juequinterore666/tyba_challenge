import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:base_app/infrastructure/core/data_source/db/firestore_database.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_database_facade.dart';
import 'package:collection/collection.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_firestore_model_data_source.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_model.dart';
import 'package:meta/meta.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

class MockDocumentSnapshot extends Mock implements DocumentSnapshot {}

class DummyModel extends IModel {
  final String email;
  final int age;

  DummyModel({String id, @required this.email, @required this.age})
      : super(id: id);

  factory DummyModel.fromJson(Map<String, dynamic> json) {
    final id = json['id'] as String;
    final email = json['email'] as String;
    final age = json['age'] as int;
    return DummyModel(email: email, id: id, age: age);
  }

  @override
  Map<String, dynamic> toJson() => {'id': this.id, 'email': email, 'age': age};

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DummyModel &&
          runtimeType == other.runtimeType &&
          email == other.email &&
          age == other.age;

  @override
  int get hashCode => email.hashCode ^ age.hashCode;
}

class DummyFirestoreModelDataSource
    extends IFirestoreModelDataSource<DummyModel> {
  DummyFirestoreModelDataSource({FirestoreDatabase firestoreDatabase})
      : super(table: 'dummies', firestoreDatabase: firestoreDatabase);

  @override
  DummyModel fromJson(Map<String, dynamic> json) {
    return DummyModel.fromJson(json);
  }
}

class MockFirestoreDatabase extends Mock implements FirestoreDatabase {}

const dummyModelsJson = [
  {'id': '1', 'email': 'juequinterore666@gmail.com', 'age': 25},
  {'id': '2', 'email': 'juequinterore6662@gmail.com', 'age': 26},
  {'id': '3', 'email': 'juequinterore6663@gmail.com', 'age': 27},
  {'id': '4', 'email': 'juequinterore6664@gmail.com', 'age': 28},
];

void main() {
  IFirestoreModelDataSource<DummyModel> firestoreModelDataSource;
  FirestoreDatabase firestoreDatabase;

  setUp(() {
    firestoreDatabase = MockFirestoreDatabase();
    firestoreModelDataSource =
        DummyFirestoreModelDataSource(firestoreDatabase: firestoreDatabase);
  });

  group('redirect calls to firestoreDatabase', () {
    test('should call firestoreDatabase create', () {
      final dummyModel =
          DummyModel(email: 'juequinterore666@gmail.com', age: 28);
      firestoreModelDataSource.create(dummyModel);

      final expectedValue = dummyModel.toJson();

      expectedValue['creationDate'] = FieldValue.serverTimestamp();
      expectedValue['isDeleted'] = false;

      verify(firestoreDatabase.create(table: 'dummies', model: expectedValue));
    });

    test('should call firestoreDatabase model', () async {
      final id1 = dummyModelsJson.firstWhere((element) => element['id'] == '1');

      when(firestoreDatabase.model(table: 'dummies', id: '1'))
          .thenAnswer((realInvocation) => Stream.value(some(id1)));

      firestoreModelDataSource.model('1');

      verify(firestoreDatabase.model(
        table: 'dummies',
        id: '1',
      ));
    });

    test(
        'should call firestoreDatabase modelsWhere with isDeleted: false if ignoreDeleted is true',
        () async {
      const queryClauses = [
        IQueryClause.where(
            field: 'age',
            value: 25,
            comparator: QueryClauseComparator.greaterThan),
        IQueryClause.limit(amount: 2)
      ];

      final expectedDocumentSnapshot = MockDocumentSnapshot();
      final expectedIterable = dummyModelsJson
          .where((element) => element['age'] as int > 25)
          .take(2);

      final paginatedModel = PaginatedModels(
          nextSkip: expectedDocumentSnapshot, models: expectedIterable);

      when(firestoreDatabase.modelsWhere(
              table: 'dummies', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((realInvocation) => Stream.value(paginatedModel));

      firestoreModelDataSource.modelsWhere(queryClauses, ignoreDeleted: true);
      final expectedQueryClauses = List.of(queryClauses);
      expectedQueryClauses.add(const IQueryClause.where(
          field: 'isDeleted',
          value: false,
          comparator: QueryClauseComparator.equals));
      verify(firestoreDatabase.modelsWhere(
          table: 'dummies', queryClauses: expectedQueryClauses));
    });

    test(
        'should call firestoreDatabase modelsWhere with original queryClauses if ignoreDeleted is false',
        () async {
      const queryClauses = [
        IQueryClause.where(
            field: 'age',
            value: 25,
            comparator: QueryClauseComparator.greaterThan),
        IQueryClause.limit(amount: 2)
      ];

      final expectedDocumentSnapshot = MockDocumentSnapshot();
      final expectedIterable = dummyModelsJson
          .where((element) => element['age'] as int > 25)
          .take(2);

      final paginatedModel = PaginatedModels(
          nextSkip: expectedDocumentSnapshot, models: expectedIterable);

      when(firestoreDatabase.modelsWhere(
              table: 'dummies', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((realInvocation) => Stream.value(paginatedModel));

      firestoreModelDataSource.modelsWhere(queryClauses, ignoreDeleted: false);

      verify(firestoreDatabase.modelsWhere(
          table: 'dummies', queryClauses: queryClauses));
    });

    test('should call firestoreDatabase modelsWhere with skip queryClause',
        () async {
      const queryClauses = [
        IQueryClause.where(
            field: 'age',
            value: 25,
            comparator: QueryClauseComparator.greaterThan),
        IQueryClause.limit(amount: 2)
      ];

      final expectedDocumentSnapshot = MockDocumentSnapshot();
      final expectedIterable = dummyModelsJson
          .where((element) => element['age'] as int > 25)
          .take(2);

      final paginatedModel = PaginatedModels(
          nextSkip: expectedDocumentSnapshot, models: expectedIterable);

      when(firestoreDatabase.modelsWhere(
              table: 'dummies', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((realInvocation) => Stream.value(paginatedModel));

      final initialStream = firestoreModelDataSource.modelsWhere(queryClauses);
      await initialStream.first;

      firestoreModelDataSource.next(queryClauses);

      //TODO: I don't know how to check that the last query is a skip with expectedDocumentSnapshot
      verify(firestoreDatabase.modelsWhere(
          table: 'dummies',
          queryClauses: argThat(allOf(hasLength(3)), named: 'queryClauses')));
    });

    test('should call firestoreDatabase update with new lastUpdatedDate', () {
      firestoreModelDataSource.update(
          id: '1', dataToUpdate: {'email': 'juequinterore666@gmail.com'});

      verify(firestoreDatabase.update(table: 'dummies', id: '1', model: {
        'email': 'juequinterore666@gmail.com',
        'lastUpdatedDate': FieldValue.serverTimestamp()
      }));
    });

    test(
        'should call firestoreDatabase delete with new deletedDate and lastUpdatedDate',
        () {
      firestoreModelDataSource.delete('1');

      verify(firestoreDatabase.update(table: 'dummies', id: '1', model: {
        'lastUpdatedDate': FieldValue.serverTimestamp(),
        'deletedDate': FieldValue.serverTimestamp(),
        'isDeleted': true
      }));
    });
  });

  group('returned values', () {
    test('should return a built DummyValue', () async {
      final id1 = dummyModelsJson.firstWhere((element) => element['id'] == '1');

      when(firestoreDatabase.model(table: 'dummies', id: '1'))
          .thenAnswer((realInvocation) => Stream.value(some(id1)));

      final modelsStream = firestoreModelDataSource.model('1');

      await for (final model in modelsStream) {
        expect(model.getOrElse(() => null), equals(DummyModel.fromJson(id1)));
      }
    });

    test('should return none if no document exists with given id', () async {
      when(firestoreDatabase.model(table: 'dummies', id: '1'))
          .thenAnswer((_) => Stream.value(none()));

      final modelsStream = firestoreModelDataSource.model('1');

      await for (final model in modelsStream) {
        expect(model, equals(none()));
      }
    });

    test('should return an Iterable<DummyModel>', () async {
      const queryClauses = [
        IQueryClause.where(
            field: 'age',
            value: 25,
            comparator: QueryClauseComparator.greaterThan),
        IQueryClause.limit(amount: 2)
      ];

      final expectedDocumentSnapshot = MockDocumentSnapshot();
      final expectedIterable = dummyModelsJson
          .where((element) => element['age'] as int > 25)
          .take(2);

      final paginatedModel = PaginatedModels(
          nextSkip: expectedDocumentSnapshot, models: expectedIterable);

      when(firestoreDatabase.modelsWhere(
              table: 'dummies', queryClauses: anyNamed('queryClauses')))
          .thenAnswer((realInvocation) => Stream.value(paginatedModel));

      final stream = firestoreModelDataSource.modelsWhere(queryClauses);
      var index = 0;
      await for (final models in stream) {
        const equalityChecker = MapEquality();
        for (final model in models) {
          expect(
              equalityChecker.equals(
                  model.toJson(), expectedIterable.toList()[index]),
              isTrue);
          index++;
        }
      }
    });
  });
}
