import 'package:base_app/infrastructure/core/data_source/db/http/base_app_post_office.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:mockito/mockito.dart';
import 'package:base_app/infrastructure/core/data_source/db/i_firestore_crud.dart';
import 'package:base_app/infrastructure/core/data_source/db/query_clause/i_query_clause.dart';

const users = [
  {
    'id': '1',
    'name': 'juequinterore1',
    'email': 'juequinterore6661@gmail.com',
    'age': 27
  },
  {
    'id': '2',
    'name': 'juequinterore2',
    'email': 'juequinterore6662@gmail.com',
    'age': 30
  },
  {
    'id': '3',
    'name': 'juequinterore3',
    'email': 'juequinterore6663@gmail.com',
    'age': 31
  },
  {
    'id': '4',
    'name': 'juequinterore4',
    'email': 'juequinterore6664@gmail.com',
    'age': 31
  },
  {
    'id': '5',
    'name': 'juequinterore5',
    'email': 'juequinterore6665@gmail.com',
    'age': 31
  },
  {
    'id': '6',
    'name': 'juequinterore6',
    'email': 'juequinterore6666@gmail.com',
    'age': 32
  },
  {
    'id': '7',
    'name': 'juequinterore7',
    'email': 'juequinterore6667@gmail.com',
    'age': 33
  },
  {
    'id': '8',
    'name': 'juequinterore8',
    'email': 'juequinterore6668@gmail.com',
    'age': 34
  },
  {
    'id': '9',
    'name': 'juequinterore9',
    'email': 'juequinterore6669@gmail.com',
    'age': 35
  },
  {
    'id': '10',
    'name': 'juequinterore10',
    'email': 'juequinterore66610@gmail.com',
    'age': 36
  },
];

const newUser = {
  'id': '100',
  'name': 'juequinterore100',
  'email': 'juequinterore666@gmail.com',
  'age': 100
};

class MockFirestore extends Mock implements Firestore {}

class MockCollectionReference extends Mock implements CollectionReference {}

class MockDocumentReference extends Mock implements DocumentReference {}

void main() {
  FirestoreCrud firestoreCrud;
  MockFirestoreInstance firestore;
  setUp(() {
    firestore = MockFirestoreInstance();
    firestoreCrud = FirestoreCrud(firestore);

    for (final user in users) {
      firestore
          .collection('users')
          .document(user['id'] as String)
          .setData(user);
    }
  });

  test('should generate id', () async {
    final mockCollectionRef = MockCollectionReference();
    final mockDocumentRef = MockDocumentReference();
    when(mockCollectionRef.document()).thenReturn(mockDocumentRef);

    final generatedId = firestoreCrud.generateId('users');

    expect(generatedId, isNotNull);
  });

  test('should create document', () async {
    final generatedId =
        await firestoreCrud.create(path: 'users', model: newUser);

    final documentReference =
        await firestoreCrud.document(path: 'users', id: generatedId).first;

    expect(documentReference.exists, isTrue);
    expect(documentReference.documentID, equals(generatedId));
  });

  test('should create document with customId', () async {
    await firestoreCrud.createWithCustomId(
        path: 'users', id: '100', model: newUser);

    final user = await firestoreCrud.document(path: 'users', id: '100').first;
    expect(user, isNotNull);
    expect(user.documentID, equals('100'));
  });

  test('should create document when upsert', () async {
    await firestoreCrud.upsert(path: 'users', id: '100', model: newUser);

    final user = await firestoreCrud.document(path: 'users', id: '100').first;

    expect(user, isNotNull);
    expect(user.documentID, equals('100'));
  });

  test('should get collectionReference', () {
    final collectionReference = firestoreCrud.getCollection(path: 'users');
    expect(collectionReference.path, 'users');
  });

  test('should query document by id', () async {
    final user = await firestoreCrud.document(path: 'users', id: '1').first;
    expect(user.data['id'], '1');
  });

  test('should throw UnsupportedError if using notEquals IQueryClause', () {
    const iQueryClauses = [
      IQueryClause.where(
          value: 25, comparator: QueryClauseComparator.notEquals, field: 'age'),
      IQueryClause.limit(amount: 3),
      IQueryClause.sort(field: 'age', order: SortQueryClauseOrder.desc),
    ];

    expect(
        () => firestoreCrud.documentsWhere(
            path: 'users', queryClauses: iQueryClauses),
        throwsUnsupportedError);
  });

  test('should query with passed iQueryClauses', () {
    const iQueryClauses = [
      IQueryClause.where(
          value: 25,
          comparator: QueryClauseComparator.greaterThan,
          field: 'age'),
      IQueryClause.limit(amount: 3),
      IQueryClause.sort(field: 'age', order: SortQueryClauseOrder.desc),
    ];

    firestoreCrud
        .documentsWhere(path: 'users', queryClauses: iQueryClauses)
        .listen((event) {
      expect(event.documents.map((e) => e.data), [
        {
          'id': '3',
          'name': 'juequinterore3',
          'email': 'juequinterore6663@gmail.com',
          'age': 31
        },
        {
          'id': '2',
          'name': 'juequinterore2',
          'email': 'juequinterore6662@gmail.com',
          'age': 30
        },
        {
          'id': '1',
          'name': 'juequinterore1',
          'email': 'juequinterore6661@gmail.com',
          'age': 27
        },
      ]);
    });
  });

  test('should update document without replacing when using upsert', () async {
    firestoreCrud.upsert(path: 'users', id: '1', model: {'age': 100});
    final userSnapshot =
        await firestoreCrud.document(path: 'users', id: '1').first;
    final user = userSnapshot.data;
    expect(user['id'], equals('1'));
    expect(user['age'], equals(100));
  });

  test('should update document without replacing', () async {
    firestoreCrud.update(path: 'users', id: '1', model: {'age': 100});
    final userSnapshot =
        await firestoreCrud.document(path: 'users', id: '1').first;
    final user = userSnapshot.data;
    expect(user['id'], equals('1'));
    expect(user['age'], equals(100));
  });

  test('should delete model with id', () async {
    await firestoreCrud.delete(path: 'users', id: '1');
    final allUsers = (await firestore.collection('users').getDocuments())
        .documents
        .map((e) => e.data);
    final expectedUsers =
        List.of(users).where((element) => element['id'] != '1');

    expect(allUsers.length, equals(expectedUsers.length));
    expect(allUsers, equals(expectedUsers));
  });

  test('should delete model where condition', () async {
    final a = await BaseAppPostOffice.test();

    const iQueryClauses = [
      IQueryClause.where(
          value: 31,
          comparator: QueryClauseComparator.greaterThan,
          field: 'age'),
    ];

    await firestoreCrud.deleteWhere(path: 'users', queryClauses: iQueryClauses);
    final allUsers = (await firestore.collection('users').getDocuments())
        .documents
        .map((e) => e.data);
    final expectedUsers =
        List.of(users).where((element) => element['age'] as int <= 31);

    expect(allUsers.length, equals(expectedUsers.length));
    expect(allUsers, equals(expectedUsers));
  });
}
