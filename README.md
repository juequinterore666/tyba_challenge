# Tyba Challenge (Frontend Engineer)

Aplicación reto para Tyba

# El código final está en la rama -> **_tyba_** <-

Este README lo edito como presentación del proyecto luego de la terminación de la prueba. Solo este README fue modificado.

### ¡Muchas gracias por darme la oportunidad de una nueva revisión del código!

#### Las características implementadas del reto fueron:
- Registro de usuario (Usando cuenta de Google)
- Inicio de sesión (Usando cuenta de Google)
- Cuando el usuario tiene la sesión iniciada:
    - Búsqueda de restaurantes por ciudad.
    - Visualización de restaurantes por ciudad (Limitado a 15 restaurantes)

#### Para el desarrollo de la aplicación usé algunas librerías desarrolladas por mí
- **Post Office**: para estandarizar las peticiones HTTP. Se encuentra alojada en un repositorio propio público y enlazada apropiadamente en el pubspec.yaml.
- **Firestore**: una librería (sin nombre) para facilitar el manejo y la conexión en tiempo real con Firestore de manera que la integración en los repositorios sea mucho más directa y se maneje paginación sin código adicional. Está incluida directamente en el proyecto.
- **PointsLoader**: animación de carga compuesta por un conjunto de puntos que forman diferentes figuras geométricas.

Entre otras librerías usadas para el desarrollo se encuentran:
- Get It: Contenedor de inyección de dependencias para desacoplar los diferentes módulos de la aplicación.
 - Bloc: Intermediario entre la vista y el dominio. Se encarga de manejar los estados de la aplicación.


#### Las características no implementadas del reto fueron:
- Cerrar sesión: El botón está incluido pero no está enlazado a la lógica de cierre de sesión. 
La lógica de cierre de sesión sí está implementada en FirebaseAuthFacade. Presionar el botón puede ocasionar el cierre de la aplicación
- Histórico de búsquedas: El histórico no se trae desde Firestore debido a un error en el nombre del campo que uso para traer la información.

### Registros gráficos de la aplicación


### Registro/Inicio de sesión
<img alt="Restaurantes en New York" src="https://drive.google.com/uc?id=1HWpEMpOCHIQUwwevBXucMiF0xoZH1JGK" width="480">

### Restaurantes en Nueva York
<img alt="Restaurantes en New York" src="https://drive.google.com/uc?id=1Rf2R2ZkRH4BlgEK8xa2JhA9_ZBOtCw73" width="480">

### Video de la búsqueda
Un video incluyendo el proceso de búsqueda puede ser visto en este enlace:

https://drive.google.com/file/d/1VppR6_NQyA9aeZgv7ug2c-YBHDZW9w8w/view?usp=sharing
<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1VppR6_NQyA9aeZgv7ug2c-YBHDZW9w8w/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
![Video búsqueda](https://drive.google.com/uc?id=1VppR6_NQyA9aeZgv7ug2c-YBHDZW9w8w)


## Entorno de desarrollo
Todo el desarrollo se lo realicé en el emulador de iOS

El proyecto fue desarrollado en Android Studio 4.0<br>

Build #AI-193.6911.18.40.6514223, built on May 20, 2020<br>
Runtime version: 1.8.0_242-release-1644-b3-6222593 x86_64<br>
VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o<br>
macOS 10.15.5

El comando flutter --version en la terminal de Android Studio arroja:<br>
Flutter 1.17.5 • channel unknown • unknown source<br>
Framework • revision 8af6b2f038 (8 weeks ago) • 2020-06-30 12:53:55 -0700<br>
Engine • revision ee76268252<br>
Tools • Dart 2.8.4<br>

